function snapCrackle(maxValue) {


    let string = ""

    for (let index = 1; index <= maxValue; index += 1) {
        if (index % 5 === 0 && index % 2 !== 0) {
            string += "SnapCrackle, "

        } else if (index % 5 === 0) {
            string += "Crackle, "

        } else if (index % 2 !== 0) {
            string += "Snap, "

        } else if (index % 5 !== 0 && index % 2 === 0) {
            string += index + ", "
        } else {
            string += index

        }


    }

    return console.log(string);
}